var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/greetings', function(req, res, next) {
    // disable Chrome XSS protection to allow demo to work
    res.set("X-XSS-Protection","0");
    res.render('greetings', { user: req.body.name });

});

module.exports = router;
